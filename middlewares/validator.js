const emailRegexCheck = email => /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email);

module.exports = {
	// Check if email is valid
	validateEmail: (req, res, next) => {
		if (req.body.email && emailRegexCheck(req.body.email)) {
			next();
		} else {
			res.status(400).json({ error: "Invalid Email Address" });
		}
	},
	// Check if email already exists
	emailExists: (req, res, next) => {
		const emailData = req.app.locals.data.find(x => x.email === req.body.email);
		if (emailData) {
			res.status(400).json({ error: "Email Already Exists" });
		} else {
			next();
		}
	}
};