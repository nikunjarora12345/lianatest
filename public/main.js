"use strict";

// Extending FormData for conversion to JSON
// Implementing toJSON function makes it compatible with JSON.stringify
FormData.prototype.toJSON = function() {
	let json = {};
	for (let pair of this.entries()) {
		json[pair[0]] = pair[1];
	}

	return json;
}

// Fetch all verified emails from server and fill in the list
const fetchData = () => {
	fetch("/api")
	.then(res => res.json())
	.then(json => {
		const listEl = document.querySelector(".list");
		listEl.innerHTML = "";

		for (let data of json) {
			let li = document.createElement("li");
			li.textContent = data.email;
			listEl.appendChild(li);
		}
	})
	.catch(err => console.error(err));
}

// Test for a regular expression for email
const regexTest = text => /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(text);

let form = null;

// Prevent default form submission and convert the form data to json
const onFormSubmit = e => {
	e.preventDefault();
	
	const formData = new FormData(form);
	if (!regexTest(formData.get("email"))) {
		alert("Invalid Email!");
		return;
	}

	const submitButton = document.querySelector(".submit");

	// Disable button and change text
	submitButton.disabled = true;
	submitButton.value = "Loading...";

	// Send request
	fetch("/api/add", {
		method: "POST",
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(formData)
	})
	.then(res => res.json())
	.then(json => {
		if (json.error) {
			alert(json.error);
		} else {
			document.querySelector(".info").innerHTML = `
				Mail sent successfully. Check it <a target="_blank" href="${json.url}">Here</a>
				<span id="close">x</span>
			`;
			document.querySelector("#close").onclick = () => {
				document.querySelector(".info").innerHTML = "";
			}

			// Enable button and change text
			submitButton.disabled = false;
			submitButton.value = "Send";

			fetchData();
		}
	})
	.catch(err => console.error(err));

	form.reset();
}

function main() {
	fetchData();

	form = document.forms[0]; 

	form.onsubmit = onFormSubmit;
}

window.onload = main;