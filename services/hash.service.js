const bcrypt = require('bcryptjs');

const saltRounds = 10;

// Helper functions for hashing and validating a string
module.exports = {
	hashText: text => bcrypt.hash(text, saltRounds),
	compareText: (text, hash) => bcrypt.compare(text, hash)
};