// Simulate a database structure to be stored in local memory
module.exports = class {
	constructor(id, email, code, verified = false) {
		this.id = id;
		this.email = email;
		this.code = code;
		this.verified = verified;
	}

	verifyEmail(email, code) {
		if (email === this.email && code === this.code) {
			this.verified = true;
			return true;
		} else {
			return false;
		}
	}
};