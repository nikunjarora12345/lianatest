const nodemailer = require('nodemailer');

/*
 * For testing purposes, we create a test SMTP account using the ethereal.email service
 * This service creates an instant SMTP account which can be used for sending mail
 * The email however is not actually sent, instead a url is created which shows the mail sent
 * In production, the test account can be replaced with an actual SMTP credentials and the rest works the same
 */

module.exports = {
	sendMail: async (req, email, code) => {
		// Create a test SMTP account from ethereal.email
		const testAcc = await nodemailer.createTestAccount();

		const transporter = nodemailer.createTransport({
			host: "smtp.ethereal.email",
			port: 587,
			secure: false,
			auth: {
				user: testAcc.user,
				pass: testAcc.pass
			}
		});

		// HTML email body
		let body = "Please verify your email by following the link: <br><a href='";
		body += req.protocol + "://" + req.get("host");
		body += "/api/verify?email=" + encodeURIComponent(email) + "&code=" + encodeURIComponent(code);
		body += "'>Click Here</a>";

		// Isn't actually sent
		const info = await transporter.sendMail({
			from: testAcc.user,
			to: email,
			subject: "Verify Email",
			html: body
		});

		console.log("Mail sent: %s", info);

		// Get the url of the generated email sent to the user
		return nodemailer.getTestMessageUrl(info);
	}
};