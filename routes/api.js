const express = require('express');
const router = express.Router();

const validator = require("../middlewares/validator");

const hashService = require('../services/hash.service');
const mailService = require('../services/email.service');
const dataService = require('../services/data.service');

// Return all verified stored data
router.get('/', (req, res, next) => {
	res.status(200).json(req.app.locals.data.filter(x => x.verified));
});

// Verify email
router.get('/verify', (req, res, next) => {
	const email = decodeURIComponent(req.query.email);
	const code = decodeURIComponent(req.query.code);

	hashService.compareText(email, code)
	.then(result => {
		if (result) {
			const data = req.app.locals.data.find(x => x.email === email);
			data.verifyEmail(email, code);
		}

		res.redirect("/");
	});
});

// Add new email
// Two middlewares to check if email is valid and does not already exist
router.post('/add', validator.validateEmail, validator.emailExists, (req, res, next) => {
	// Get id of last stored data
	const dataSize = req.app.locals.data.length;
	let lastId = 0;

	if (dataSize > 0) {
		lastId = req.app.locals.data[dataSize - 1].id;
	}

	// Hash email
	hashService.hashText(req.body.email)
	.then(hash => {
		// Add to database
		const newData = new dataService(lastId + 1, req.body.email, hash);
		req.app.locals.data.push(newData);

		return mailService.sendMail(req, newData.email, newData.code);
	})
	.then(url => {
		// Send back the url to the email message
		res.status(200).json({ message: "Added successfully", url: url });
	})
	.catch(err => {
		res.status(500).json({ error: "Internal Server Error" });
		console.error(err);
	});
});

module.exports = router;
