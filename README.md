# Email System

Steps for running:
- `git clone https://gitlab.com/nikunjarora12345/lianatest.git`
- `cd lianatest`
- `npm install`
- `npm run start`

Default port is 8000. So it should run at http://localhost:8000

Add an email address in the email field

![form](public/images/form.jpg)

You should get a banner at the top notifying that an email has been sent and a link to the email
PS: This uses the [ethereal.email](https://ethereal.email/) service which creates a test SMTP account on the fly and sends a dummy email which can be previewed at a url. No actual mail is sent.

![info](public/images/info.jpg)

Clicking on the link on the banner takes you to the email page where you can click the link in the email body to verify your email. In production, this same email will be sent to your actual email account.

![email](public/images/email.jpg)

The link will redirect you to the home page, where now it'll show an item appended to the list at the bottom. This keeps track of all emails that have been verified.

![list](public/images/list.jpg)
